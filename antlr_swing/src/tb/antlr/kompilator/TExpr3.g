tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) 
"<deklaracje;separator=\" \n\">
start:
    <name;separator=\" \n\">
"
        ;

decl    
        : ^(VAR name=ID)  {symbols.newSymbol($name.text);} 
            -> declaration(n={symbols.getNameAndDepth($ID.text)})    
        ;


expr    : ^(PLUS  e1=expr e2=expr)  {} -> plus(p1={$e1.st},p2={$e2.st})
        | LB                        {symbols.enterScope();}
        | RB                        {symbols.leaveScope();}
        | ^(MINUS e1=expr e2=expr)  {} -> minus(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)  {} -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)  {} -> div(p1={$e1.st},p2={$e2.st})
        | (name=ID)                 {} -> getVar(v={symbols.getNameAndDepth($ID.text)})
        | ^(PODST name=ID e1=expr)  {} -> setVar(name={symbols.getNameAndDepth($ID.text)}, v={$e1.st})
        | INT                       {} -> int(i={$INT.text})
    ;
    
catch [RuntimeException ex] {err(ex);}
    